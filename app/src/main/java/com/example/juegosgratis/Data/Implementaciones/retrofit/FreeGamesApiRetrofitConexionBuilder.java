package com.example.juegosgratis.Data.Implementaciones.retrofit;

import com.example.juegosgratis.Data.Adaptadores.DeserializadorFullGame;
import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.Juego;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FreeGamesApiRetrofitConexionBuilder {

    public static Retrofit obtenerConexion(Gson gson){
        Retrofit retrofit = null;
        GsonConverterFactory gsonConverterFactory;
        if(gson == null){
            gsonConverterFactory = GsonConverterFactory.create();
        }else{
            gsonConverterFactory = GsonConverterFactory.create(gson);
        }

        Retrofit.Builder retrofilBuilder = new Retrofit.Builder();
        retrofilBuilder.baseUrl(ConstantesApiFreeGame.URL_BASICA);
        retrofilBuilder.addConverterFactory(gsonConverterFactory);
        return retrofilBuilder.build();
    }

    public static Gson obtenerDeserealizadorDeJuegoCompleto(Juego game){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(JuegoConDetalles.class, new DeserializadorFullGame(game));
        return gsonBuilder.create();
    }
}
