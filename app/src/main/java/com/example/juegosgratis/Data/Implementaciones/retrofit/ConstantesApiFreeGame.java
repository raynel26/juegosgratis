package com.example.juegosgratis.Data.Implementaciones.retrofit;

public class ConstantesApiFreeGame {
    public static String URL_BASICA = "https://www.freetogame.com/";

    public final static String GAMES = "/games";
    public final static String GAMES_PLATAFORMA = "/games?platform={pc}";
    public final static String GAMES_CATEGORIA = "/games?category={shooter}";
    public final static String GAMES_ID = "/game?id={452}";

    public static String LINK_PETICION_CATEGORIA = "";

    public final static void getCategoria(String categoria){
        LINK_PETICION_CATEGORIA = "games?category=" + categoria;
    }
}
