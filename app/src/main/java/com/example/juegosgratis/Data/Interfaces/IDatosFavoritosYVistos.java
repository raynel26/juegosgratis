package com.example.juegosgratis.Data.Interfaces;

import com.example.juegosgratis.Model.BusquedaReciente;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.ArrayList;

public interface IDatosFavoritosYVistos {

    public void agregarJuegoAFavotitos(JuegoParaFavorito j);

    public void agregarJuegoAVistosRecientemente(JuegoParaFavorito j);

    public void eliminarJuegoDeFavotitos(int idJuego);

    public void eliminarJuegoDeVistosRecienteMente(int idJuego);

    public ArrayList<JuegoParaFavorito> obtenerJuegosFavoritos();

    public ArrayList<JuegoParaFavorito> obtenerJuegosVistosRecientemente();

    public void agregarBusquedaReciente(String busqueda);

    public void eliminarBusquedaReciente(BusquedaReciente busqueda);

    public ArrayList<BusquedaReciente> obtenerBusquedasRecientes();

}
