package com.example.juegosgratis.Data.Interfaces;

import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.Juego;

import java.util.List;

public interface IDataEndPoints {

    JuegoConDetalles obtenerJuegoConDetalles(Juego juego);

    JuegoConDetalles obtenerJuegoConDetalles(int idjuego);

    List<Juego> obtenerTodosLosJuegos();

    List<Juego> obtenerJuegosPorPlataforma(String platforma);

    List<Juego> obtenerJuegosPorCategoria(String categoria);

    List<Juego> obtenerJuegosOrdenadosPor(String orden);
}
