package com.example.juegosgratis.Data.BaseDeDatosLocal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.juegosgratis.Data.Interfaces.IDatosFavoritosYVistos;
import com.example.juegosgratis.Model.BusquedaReciente;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.ArrayList;

public class FuenteDeDatosFavoritosYVistos implements IDatosFavoritosYVistos {
    public FuenteDeDatosFavoritosYVistos(Context context) {
        baseDeDatos = InstanciaBaseDeDatos.obtenerConexionBaseDeDato(context);
    }

    @Override
    public void agregarJuegoAFavotitos(JuegoParaFavorito j) {
        agregar(j, ConstantesBaseDeDatos.TABLE_FAVORITOS_NOMBRE);
    }

    @Override
    public void agregarJuegoAVistosRecientemente(JuegoParaFavorito j) {
        agregar(j, ConstantesBaseDeDatos.TABLE_VISTOSRECIENTEMENTE_NOMBRE);
    }

    @Override
    public void eliminarJuegoDeFavotitos(int idJuego) {
        SQLiteDatabase writer = baseDeDatos.getWritableDatabase();
        writer.execSQL(ConstantesBaseDeDatos.queryParaELiminarRegistro(ConstantesBaseDeDatos.TABLE_FAVORITOS_NOMBRE, idJuego));
        writer.close();
    }

    @Override
    public void eliminarJuegoDeVistosRecienteMente(int idJuego) {
        SQLiteDatabase writer = baseDeDatos.getWritableDatabase();
        writer.execSQL(ConstantesBaseDeDatos.queryParaELiminarRegistro(ConstantesBaseDeDatos.TABLE_VISTOSRECIENTEMENTE_NOMBRE, idJuego));
        writer.close();
    }

    @Override
    public ArrayList<JuegoParaFavorito> obtenerJuegosFavoritos() {
        return obtener(ConstantesBaseDeDatos.queryObtenerTabla(ConstantesBaseDeDatos.TABLE_FAVORITOS_NOMBRE));
    }

    @Override
    public ArrayList<JuegoParaFavorito> obtenerJuegosVistosRecientemente() {
        return obtener(ConstantesBaseDeDatos.queryObtenerTabla(ConstantesBaseDeDatos.TABLE_VISTOSRECIENTEMENTE_NOMBRE));
    }

    @Override
    public void agregarBusquedaReciente(String busqueda) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ConstantesBaseDeDatos.CAMPO_BUSQUEDA, busqueda);

        SQLiteDatabase writer = baseDeDatos.getWritableDatabase();
        writer.insert(ConstantesBaseDeDatos.TABLE_BUSQUEDASRECIENTES_NOMBRE, null, contentValues);
        baseDeDatos.close();
    }

    @Override
    public void eliminarBusquedaReciente(BusquedaReciente busqueda) {
        SQLiteDatabase writer = baseDeDatos.getWritableDatabase();
        writer.execSQL(ConstantesBaseDeDatos.queryParaELiminarRegistro(ConstantesBaseDeDatos.TABLE_BUSQUEDASRECIENTES_NOMBRE, busqueda.getId()));
        writer.close();
    }

    @Override
    public ArrayList<BusquedaReciente> obtenerBusquedasRecientes() {
        SQLiteDatabase reader = baseDeDatos.getReadableDatabase();
        ArrayList<BusquedaReciente> busquedaRecientes;
        try (Cursor cursor = reader.rawQuery(ConstantesBaseDeDatos.queryObtenerTabla(ConstantesBaseDeDatos.TABLE_BUSQUEDASRECIENTES_NOMBRE), null)) {
            busquedaRecientes = new ArrayList<>(cursor.getCount());

            int i = 0;
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String busqueda = cursor.getString(1);

                busquedaRecientes.add(i, new BusquedaReciente(id, busqueda));
                i++;
            }
        }
        return busquedaRecientes;
    }

    private void agregar(JuegoParaFavorito j, String tabla){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ConstantesBaseDeDatos.CAMPO_ID_JUEGO, j.getId());
        contentValues.put(ConstantesBaseDeDatos.CAMPO_NOMBRE_JUEGO, j.getNombre());
        contentValues.put(ConstantesBaseDeDatos.CAMPO_LINKIMAGEN_JUEGO, j.getImagen());

        SQLiteDatabase writer = baseDeDatos.getWritableDatabase();
        writer.insert(tabla, null, contentValues);
        baseDeDatos.close();
    }

    private ArrayList<JuegoParaFavorito> obtener(String query){
        SQLiteDatabase reader = baseDeDatos.getReadableDatabase();
        ArrayList<JuegoParaFavorito> juegos;
        try (Cursor cursor = reader.rawQuery(query, null)) {
            juegos = new ArrayList<>(cursor.getCount());

            int i = 0;
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String nombre = cursor.getString(1);
                String linkImagen = cursor.getString(2);

                juegos.add(i, new JuegoParaFavorito(linkImagen, nombre, id));
                i++;
            }
        }
        return juegos;
    }

    public static FuenteDeDatosFavoritosYVistos instanciar(FuenteDeDatosFavoritosYVistos datosFavoritosYVistos, Context context){
        if(datosFavoritosYVistos == null){
            datosFavoritosYVistos = new FuenteDeDatosFavoritosYVistos(context);
        }
        return datosFavoritosYVistos;
    }

    private BaseDeDatosSQLITE baseDeDatos;
}
