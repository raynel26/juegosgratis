package com.example.juegosgratis.Data.Implementaciones.retrofit;

import com.example.juegosgratis.Data.Interfaces.IDataEndPoints;
import com.example.juegosgratis.Data.Interfaces.EndsPoinsJuegosApiRetrofit;
import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.Juego;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FuenteDeDatosJuegos implements IDataEndPoints{

    @Override
    public List<Juego> obtenerTodosLosJuegos() {
        return obtenerListaDeJuegos(TODOS_LOS_JUEGOS, null);
    }

    @Override
    public List<Juego> obtenerJuegosPorCategoria(String categoria) {
        return obtenerListaDeJuegos(JUEGOS_POR_CATEGORIA, categoria);
    }

    @Override
    public List<Juego> obtenerJuegosOrdenadosPor(String orden) {
        return obtenerListaDeJuegos(JUEGOS_ORDENADOS_POR, orden);
    }

    @Override
    public List<Juego> obtenerJuegosPorPlataforma(String platforma) {
        return obtenerListaDeJuegos(JUEGOS_POR_PLATAFORMA, platforma);
    }

    @Override
    public JuegoConDetalles obtenerJuegoConDetalles(Juego juego) {
        return obtenerJuego(JUEGO_PARTICULAR_USANDO_JUEGO_SIN_DETALLES, juego, null);
    }

    @Override
    public JuegoConDetalles obtenerJuegoConDetalles(int idGame) {
        return obtenerJuego(JUEGO_PARTICULAR, null, idGame);
    }

    private List<Juego> obtenerListaDeJuegos(int criterioDeBusqueda, String palabraDeBusqueda){
        EndsPoinsJuegosApiRetrofit endsPoins = obtenerInstanciaEndsPoins(null);
        ArrayList<Juego> lista_juegos = null;
        Call<ArrayList<Juego>> call = null;
        switch (criterioDeBusqueda) {
            case TODOS_LOS_JUEGOS:
                call = endsPoins.obtenerTodosLosJuegos();
                break;
            case JUEGOS_POR_CATEGORIA:
                call = endsPoins.obtenerJuegosPorCategoria(palabraDeBusqueda);
                break;
            case JUEGOS_POR_PLATAFORMA:
                break;
            case JUEGOS_ORDENADOS_POR:
                call = endsPoins.obtenerJuegosOrdenadosPor(palabraDeBusqueda);
                break;
            default:
                break;
        }
        return hacerLLamada(lista_juegos, call);
    }

    private JuegoConDetalles obtenerJuego(int tipoDeBusqueda, Juego juego, Integer idJuego){
        JuegoConDetalles fullGame = null;
        Call<JuegoConDetalles> call = null;
        Gson desearilizadorJuegoCompleto = null;

        EndsPoinsJuegosApiRetrofit endsPoins = null;
                switch (tipoDeBusqueda) {
            case JUEGO_PARTICULAR_USANDO_JUEGO_SIN_DETALLES:
                desearilizadorJuegoCompleto = FreeGamesApiRetrofitConexionBuilder.obtenerDeserealizadorDeJuegoCompleto(juego);
                endsPoins = obtenerInstanciaEndsPoins(desearilizadorJuegoCompleto);
                call = endsPoins.obtenerJuegoParticularConDetalles(juego.getId());
                break;
            case JUEGO_PARTICULAR:
                desearilizadorJuegoCompleto = FreeGamesApiRetrofitConexionBuilder.obtenerDeserealizadorDeJuegoCompleto(null);
                endsPoins = obtenerInstanciaEndsPoins(desearilizadorJuegoCompleto);
                call = endsPoins.obtenerJuegoParticularConDetalles(idJuego);
                break;
            default:
                break;
        }
        return hacerLLamada(fullGame, call);
    }

    private ArrayList<Juego> hacerLLamada(ArrayList<Juego> lista_juegos, Call<ArrayList<Juego>> call){
        try {
            Response<ArrayList<Juego>> r = call.execute();
            lista_juegos = r.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lista_juegos;
    }

    private JuegoConDetalles hacerLLamada(JuegoConDetalles fullGame,  Call<JuegoConDetalles> call){
        try {
            fullGame = call.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fullGame;
    }

    private EndsPoinsJuegosApiRetrofit obtenerInstanciaEndsPoins(Gson gson){
        Retrofit retrofit = FreeGamesApiRetrofitConexionBuilder.obtenerConexion(gson);
        return retrofit.create(EndsPoinsJuegosApiRetrofit.class);
    }

    private List<Juego> listaJuegos;

    public static final int JUEGOS_POR_CATEGORIA = 1;
    public static final int TODOS_LOS_JUEGOS = 2;
    public static final int JUEGOS_POR_PLATAFORMA = 3;
    public static final int JUEGOS_ORDENADOS_POR = 4;
    public static final int JUEGO_PARTICULAR_USANDO_JUEGO_SIN_DETALLES = 5;
    public static final int JUEGO_PARTICULAR = 6;
    public static final int JUEGOS_POR_GENERO = 7;
}
