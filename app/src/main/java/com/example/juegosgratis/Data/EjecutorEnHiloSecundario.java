package com.example.juegosgratis.Data;

import com.example.juegosgratis.Util.Filtros;
import com.example.juegosgratis.Data.Interfaces.IDataEndPoints;
import com.example.juegosgratis.Data.Interfaces.IDatosFavoritosYVistos;
import com.example.juegosgratis.Model.Juego.Juego;
import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class EjecutorEnHiloSecundario {

    public EjecutorEnHiloSecundario(IDataEndPoints fuenteDeDatos) {
        this.fuenteDeDatosJuegos = fuenteDeDatos;
    }

    public EjecutorEnHiloSecundario(IDatosFavoritosYVistos fuenteDeDatosFavoritos) {
        this.fuenteDeDatosFavoritos = fuenteDeDatosFavoritos;
    }

    public List<Juego> obtenerTodosLosJuegos() throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<List<Juego>> result = ejecutor.submit(() -> fuenteDeDatosJuegos.obtenerTodosLosJuegos());
        return result.get();
    }

    public List<Juego> obtenerJuegosPorCategoria(String genero) throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<List<Juego>> result = ejecutor.submit(() -> fuenteDeDatosJuegos.obtenerJuegosPorCategoria(genero));
        return result.get();
    }

    public List<Juego> obtenerJuegosOrdenadosPor(String orden) throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<List<Juego>> result = ejecutor.submit(() -> fuenteDeDatosJuegos.obtenerJuegosOrdenadosPor(orden));
        return result.get();
    }

    public List<Juego> obtenerJuegosPorNombre(String nombre) throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<List<Juego>> result = ejecutor.submit(() -> {
             List<Juego> todosLosJuegos = fuenteDeDatosJuegos.obtenerTodosLosJuegos();
             return Filtros.filtrarListaDeJuegosPorNombre(todosLosJuegos, nombre);
        });
        return result.get();
    }

    public List<Juego> obtenerJuegosPorPlataforma(String platforma) throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<List<Juego>> result = ejecutor.submit(() -> fuenteDeDatosJuegos.obtenerJuegosPorPlataforma(platforma));
        return result.get();
    }

    public JuegoConDetalles obtenerJuegoConDetalles(Juego juego) throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<JuegoConDetalles> result = ejecutor.submit(() -> fuenteDeDatosJuegos.obtenerJuegoConDetalles(juego));
        return result.get();
    }

    public JuegoConDetalles obtenerJuegoConDetalles(int idGame) throws ExecutionException, InterruptedException {
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        Future<JuegoConDetalles> result = ejecutor.submit(() -> fuenteDeDatosJuegos.obtenerJuegoConDetalles(idGame));
        return result.get();
    }

    public void agregarJuegoAFavotitos(JuegoParaFavorito j){
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        ejecutor.submit(() -> fuenteDeDatosFavoritos.agregarJuegoAFavotitos(j));
    }

    public void eliminarJuegoDeFavotitos(int idJuego){
        ExecutorService ejecutor = Hilo.obtenerEjecutorHilo();
        ejecutor.submit(() -> fuenteDeDatosFavoritos.eliminarJuegoDeFavotitos(idJuego));
    }

    //-------------------------------------------------------------------------------------//
    private IDataEndPoints fuenteDeDatosJuegos;
    private IDatosFavoritosYVistos fuenteDeDatosFavoritos;
}
