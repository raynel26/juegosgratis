package com.example.juegosgratis.Data.Interfaces;

import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.Juego;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EndsPoinsJuegosApiRetrofit {

    @GET("api/games")
    Call<ArrayList<Juego>> obtenerTodosLosJuegos();

    @GET("api/game")
    Call<JuegoConDetalles> obtenerJuegoParticularConDetalles(@Query("id") int id);

    @GET("games?platform={plataforma}")
    Call<ArrayList<Juego>> obtenerJuegosPorPlataforma(@Path("plataforma") String platforma);

    @GET("api/games")
    Call<ArrayList<Juego>> obtenerJuegosPorCategoria(@Query("category") String c);

    @GET("/api/games?sort-by")
    Call<ArrayList<Juego>> obtenerJuegosOrdenadosPor(@Query("orden") String orden);
}
