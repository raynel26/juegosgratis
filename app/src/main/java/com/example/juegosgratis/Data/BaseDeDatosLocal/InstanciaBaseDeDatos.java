package com.example.juegosgratis.Data.BaseDeDatosLocal;

import android.content.Context;

public class InstanciaBaseDeDatos {

    public static BaseDeDatosSQLITE obtenerConexionBaseDeDato(Context context){
        if (baseDeDatos == null){
            baseDeDatos = new BaseDeDatosSQLITE(context);
        }
        return baseDeDatos;
    }

    private static BaseDeDatosSQLITE baseDeDatos;
}
