package com.example.juegosgratis.Data.BaseDeDatosLocal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class BaseDeDatosSQLITE extends SQLiteOpenHelper {
    public BaseDeDatosSQLITE(Context context) {
        super(context, ConstantesBaseDeDatos.NOMBRE_BASE_DE_DATOS, null, ConstantesBaseDeDatos.VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ConstantesBaseDeDatos.QUERY_TABLA_FAVORITOS);
        db.execSQL(ConstantesBaseDeDatos.QUERY_TABLA_VISTOSRECIENTE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ConstantesBaseDeDatos.QUERY_TABLA_BUSQUEDAS_RECIENTES);
    }
}
