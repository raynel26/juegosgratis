package com.example.juegosgratis.ViewModel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.juegosgratis.Util.Filtros;
import com.example.juegosgratis.Data.BaseDeDatosLocal.FuenteDeDatosFavoritosYVistos;
import com.example.juegosgratis.Data.Interfaces.IDatosFavoritosYVistos;
import com.example.juegosgratis.Data.EjecutorEnHiloSecundario;
import com.example.juegosgratis.Data.Interfaces.IDataEndPoints;
import com.example.juegosgratis.Data.Implementaciones.retrofit.FuenteDeDatosJuegos;
import com.example.juegosgratis.Model.ConstantesJuego;
import com.example.juegosgratis.Model.Juego.Juego;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainViewModel extends ViewModel {

    public MainViewModel() {
        fuenteDeDatosDeJuegos = new FuenteDeDatosJuegos();
        observable_listaDeJuegosAleatorios = new MutableLiveData<>();
        obserable_listaDeJuegosPorGenero = new MutableLiveData<>();
        observable_listaTodosLosJuegos = new MutableLiveData<>();
        observable_listaJuegosMasPopulares = new MutableLiveData<>();
    }

    public void setContext(Context context){
        this.context = context;
    }

    public void obtenerTodosLosJuegos(){
        try {
            listaDeTodosLosJuegos = new EjecutorEnHiloSecundario(fuenteDeDatosDeJuegos)
                            .obtenerTodosLosJuegos();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        observable_listaTodosLosJuegos.postValue(listaDeTodosLosJuegos);
    }

    public void obtenerListaDeJuegosMasPopulares(){
        try {
            listaJuegosMasPopulares = new EjecutorEnHiloSecundario(fuenteDeDatosDeJuegos)
                    .obtenerJuegosOrdenadosPor(ConstantesJuego.MAS_POPULARES);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        observable_listaJuegosMasPopulares.postValue(listaJuegosMasPopulares);
    }

    public ArrayList<JuegoParaFavorito> obtenerFavoritos(){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        return datosFavoritosYVistos.obtenerJuegosFavoritos();
    }

    public ArrayList<JuegoParaFavorito> obtenerVistosRecientemente(){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        return datosFavoritosYVistos.obtenerJuegosVistosRecientemente();
    }

    public void eliminarJuegoDeFavotitos(int idJuego){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        new EjecutorEnHiloSecundario(datosFavoritosYVistos).eliminarJuegoDeFavotitos(idJuego);
        //datosFavoritosYVistos.eliminarJuegoDeFavotitos(idJuego);
    }

    public LiveData<List<Juego>> obtenerObservableListaJuegosMasPopulares() {
        return observable_listaJuegosMasPopulares;
    }

    public LiveData<List<Juego>> obtenerObservableListaTodosLosJuegos() {
        return observable_listaTodosLosJuegos;
    }

    public LiveData<List<Juego>> obtenerObservableListaDeJuegosAleatorios() {
        return observable_listaDeJuegosAleatorios;
    }

    public LiveData<ArrayList<List<Juego>>> obtenerObservableListaDeJuegosPorGeneros() {
        return obserable_listaDeJuegosPorGenero;
    }

    public List<String> getListaDeGenerosReducida(){
        ArrayList<String> lista = new ArrayList<>(9);
        lista.add(0, "shooter");
        lista.add(1, "mmorpg");
        lista.add(2, "strategy");
        lista.add(3, "racing");
        lista.add(4, "sports");
        lista.add(5, "survival");
        lista.add(6, "fantasy");
        lista.add(7, "action");
        lista.add(8, "All Category");

        return lista;
    }

    public List<String> getListaDeGenerosCompleta(){
        ArrayList<String> lista = new ArrayList<>();
        return lista;
    }

    private void inicializar(){
        if(datosFavoritosYVistos == null){
            datosFavoritosYVistos = new FuenteDeDatosFavoritosYVistos(context);
        }
    }

    public List<Juego> filtrarTresJuegosAleatorios(List<Juego> listaJuegos){
        listaDeJuegosAleatorios =  Filtros.filtrasTresJuegosAleatorios(listaJuegos);
        return listaDeJuegosAleatorios;
    }

    public List<Juego> filtrarPrimerosDiezJuegos(List<Juego> listaJuegos){
        listaJuegosMasPopulares = Filtros.filtrarPrimerosDiezJuegos(listaJuegos);
         return listaJuegosMasPopulares;
    }

    public void setListaDeJuegosAleatorios(List<Juego> listaDeJuegosAleatorios) {
        this.listaDeJuegosAleatorios = listaDeJuegosAleatorios;
    }

    public void setListaJuegosMasPopulares(List<Juego> listaJuegosMasPopulares) {
        this.listaJuegosMasPopulares = listaJuegosMasPopulares;
    }

    public List<Juego> getListaDeJuegosAleatorios() {
        return listaDeJuegosAleatorios;
    }

    public List<Juego> getListaJuegosMasPopulares() {
        return listaJuegosMasPopulares;
    }

    public Integer getItemJuegosAlazar() {
        return itemJuegosAlazar;
    }

    public void setItemJuegosAlazar(int itemJuegosAlazar) {
        this.itemJuegosAlazar = itemJuegosAlazar;
    }

    //-----------------------------------------------------------------------------------//
    private IDataEndPoints fuenteDeDatosDeJuegos;
    private List<Juego> listaDeTodosLosJuegos;
    private List<Juego> listaDeJuegosAleatorios;
    private List<Juego> listaJuegosMasPopulares;
    private IDatosFavoritosYVistos datosFavoritosYVistos;
    private Context context;

    private Integer itemJuegosAlazar;

    //observadores
    private MutableLiveData<List<Juego>> observable_listaJuegosMasPopulares;
    private MutableLiveData<List<Juego>> observable_listaTodosLosJuegos;
    private MutableLiveData<List<Juego>> observable_listaDeJuegosAleatorios;
    private MutableLiveData<ArrayList<List<Juego>>> obserable_listaDeJuegosPorGenero;

    private String SHOOTER = "Shooter";
    private String SPORTS = "Sports";
    private String MMORPG = "MMORPG";

    public static int CANTIDAD_JUEGOS_POPULARES = 10;
}
