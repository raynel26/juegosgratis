package com.example.juegosgratis.ViewModel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.juegosgratis.Data.BaseDeDatosLocal.FuenteDeDatosFavoritosYVistos;
import com.example.juegosgratis.Data.Interfaces.IDatosFavoritosYVistos;
import com.example.juegosgratis.Data.EjecutorEnHiloSecundario;
import com.example.juegosgratis.Data.Interfaces.IDataEndPoints;
import com.example.juegosgratis.Data.Implementaciones.retrofit.FuenteDeDatosJuegos;
import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.Juego;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class JuegosViewModel extends ViewModel {
    public JuegosViewModel() {
        fuenteDeDatosDeJuegos = new FuenteDeDatosJuegos();
        observableJuegoEnDetalle = new MutableLiveData<>();
        observableListaDeJuegos = new MutableLiveData<>();
    }

    public void setContext(Context context){
        this.context = context;
    }

    public void obtenerListaDeTodosLosJuegos(){
        try {
            listaDeJuegosActual = new EjecutorEnHiloSecundario(fuenteDeDatosDeJuegos)
                    .obtenerTodosLosJuegos();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        observableListaDeJuegos.postValue(listaDeJuegosActual);
    }

    public void obtenerListaDeJuegosPorGenero(String genero){
        try {
            listaDeJuegosActual = new EjecutorEnHiloSecundario(fuenteDeDatosDeJuegos)
                    .obtenerJuegosPorCategoria(genero);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        observableListaDeJuegos.postValue(listaDeJuegosActual);
    }

    public void obtenerListaDeJuegosPorOrden(String orden) {
        try {
            listaDeJuegosActual = new EjecutorEnHiloSecundario(fuenteDeDatosDeJuegos)
                    .obtenerJuegosOrdenadosPor(orden);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        observableListaDeJuegos.postValue(listaDeJuegosActual);
    }

    public void obtenerJuegoEnDetalle(int idGame){
        try {
            juegoEnDetalle = new EjecutorEnHiloSecundario(fuenteDeDatosDeJuegos)
                    .obtenerJuegoConDetalles(idGame);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        observableJuegoEnDetalle.postValue(juegoEnDetalle);
    }

    public MutableLiveData<List<Juego>> getObservableListaDeJuegos() {
        return observableListaDeJuegos;
    }

    public MutableLiveData<JuegoConDetalles> getObservableJuegoEnDetalle() {
        return observableJuegoEnDetalle;
    }

    public void setIndiceJuegoSeleccionadoEnListaActual(Integer juegoSeleccionado) {
        this.indiceJuegoSeleccionadoEnListaActual = juegoSeleccionado;
    }

    public List<Juego> obtenerListaDeJuegosActual() {
        return listaDeJuegosActual;
    }

    public JuegoConDetalles obtenerJuegoActual(){
        return juegoEnDetalle;
    }

    public ArrayList<JuegoParaFavorito> getFavoritos(){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos) datosFavoritosYVistos, context);
        return datosFavoritosYVistos.obtenerJuegosFavoritos();
    }

    public ArrayList<JuegoParaFavorito> getVistosRecientemente(){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        return datosFavoritosYVistos.obtenerJuegosVistosRecientemente();
    }

    public void agregarAVistosRecientemente(Juego juego){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        datosFavoritosYVistos.agregarJuegoAVistosRecientemente(new JuegoParaFavorito(juego.getThumbnail(), juego.getTitle(), juego.getId()));
    }

    public void agregarAFavoritos(Juego juego){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        new EjecutorEnHiloSecundario(datosFavoritosYVistos).agregarJuegoAFavotitos(new JuegoParaFavorito(juego.getThumbnail(), juego.getTitle(), juego.getId()));
        //datosFavoritosYVistos.agregarJuegoAFavotitos(new JuegoParaFavorito(juego.getThumbnail(), juego.getTitle(), juego.getId()));
    }

    public void eliminarJuegoDeFavoritos(int idJuego){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        new EjecutorEnHiloSecundario(datosFavoritosYVistos).eliminarJuegoDeFavotitos(idJuego);
        //datosFavoritosYVistos.eliminarJuegoDeFavotitos(idJuego);
    }

    public void eliminarJuegoDeVistosRecientemente(int idJuego){
        datosFavoritosYVistos = FuenteDeDatosFavoritosYVistos.instanciar((FuenteDeDatosFavoritosYVistos)datosFavoritosYVistos, context);
        datosFavoritosYVistos.eliminarJuegoDeVistosRecienteMente(idJuego);
    }

    //------------------------------------------------------------------//
    private IDataEndPoints fuenteDeDatosDeJuegos;
    private List<Juego> listaDeJuegosActual;
    private JuegoConDetalles juegoEnDetalle;
    private Integer indiceJuegoSeleccionadoEnListaActual;
    private IDatosFavoritosYVistos datosFavoritosYVistos;
    private Context context;

    private MutableLiveData<JuegoConDetalles> observableJuegoEnDetalle;
    private MutableLiveData<List<Juego>> observableListaDeJuegos;
}
