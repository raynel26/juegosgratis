package com.example.juegosgratis.View.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.juegosgratis.R;
import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.example.juegosgratis.Model.Juego.Juego;
import com.example.juegosgratis.Model.Juego.ScreenShot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AdapterPager_ImagenesDeslizables extends RecyclerView.Adapter<AdapterPager_ImagenesDeslizables.ViewPageHolder>{

    public AdapterPager_ImagenesDeslizables(JuegoConDetalles juego, Context context) {
        List<ScreenShot> imagenes = juego.getImagenes();
        int longitudListaImagenes = imagenes.size();
        this.linksImagenes = new ArrayList<>(longitudListaImagenes);
        for (int i = 0; i < longitudListaImagenes; i++) {
            linksImagenes.add(i, imagenes.get(i).getImage());
        }
    }

    public AdapterPager_ImagenesDeslizables(List<Juego> listaJuegos, Context context) {
        int longitudListaJuegos = listaJuegos.size();
        this.lista = listaJuegos;
        this.linksImagenes = new ArrayList<>(longitudListaJuegos);
        for (int i = 0; i < longitudListaJuegos; i++) {
            linksImagenes.add(i, listaJuegos.get(i).getThumbnail());
        }
        this.context = context;
        observablePosicion = new MutableLiveData<>();
    }

    @Override
    public ViewPageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.imagenpaginaprincipal, parent, false);
        return new ViewPageHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterPager_ImagenesDeslizables.ViewPageHolder holder, int position) {
        holder.setImagen(linksImagenes.get(position));
    }
    @Override
    public int getItemCount() {
        return linksImagenes.size();
    }

    public Context getContext() {
        return context;
    }

    public MutableLiveData<Integer> getObservablePosicion() {
        return observablePosicion;
    }

    class ViewPageHolder extends RecyclerView.ViewHolder {
        public ViewPageHolder(View itemView) {
            super(itemView);
            imagenView = itemView.findViewById(R.id.iv_imagenPaginaPrincipal);
        }

        public void setImagen(String linkImagen){
            //int w = imagenView.getDrawable().getIntrinsicWidth();
            //int h = imagenView.getDrawable().getIntrinsicHeight();
            //Picasso.with(context)
            //        .load(linkImagen)
            //        .resize(50, 50)
            //        .centerCrop()
            //        .into(imagenView);
            Picasso.with(context).load(linkImagen).into(imagenView);
        }
        private ImageView imagenView;
        private FrameLayout fondo;
    }

    private MutableLiveData<Integer> observablePosicion;
    private List<Juego> lista;
    private List<String> linksImagenes;
    private Context context;
}
