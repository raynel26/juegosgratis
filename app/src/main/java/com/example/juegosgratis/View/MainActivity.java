package com.example.juegosgratis.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.util.Log;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;

import com.example.juegosgratis.R;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
    public static final String BUSQUEDA_POR_ORDEN = "pororden";
    public static final String ORDEN = "orden";
    String TAG = "ACTIVIDAD PRINCIPAL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        habilitarTransiciones();
        setContentView(R.layout.activity_main);
        referenciarViews();
        configData();
        cambiarFragmentTab(TAB_INICIO);
        setSupportActionBar(toolbar);
    }

    public void abrirDetalleDeJuegoPorId(int idJuego){
        Intent intent = new Intent(getApplicationContext(), JuegosActivity.class);
        intent.putExtra(ACCION, ABRIR_JUEGO_EN_DETALLE);
        intent.putExtra(JuegosActivity.ID_JUEGO, idJuego);
        intent.putExtra(JuegosActivity.ORIGEN, JuegosActivity.FAVORITOS);

        activarTransicion(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    public void abrirListaDeJuegosPorGenero(String genero){
        Intent intent = new Intent(getApplicationContext(), JuegosActivity.class);
        intent.putExtra(ACCION, ABRIR_LISTA_DE_JUEGOS);
        intent.putExtra(TIPO_DE_BUSQUEDA, BUSQUEDA_POR_GENERO);;
        intent.putExtra(MainActivity.GENERO, genero);

        activarTransicion(intent);
    }

    public void abrirListaDeJuegosPorOrden(String orden){
        Intent intent = new Intent(getApplicationContext(), JuegosActivity.class);
        intent.putExtra(ACCION, ABRIR_LISTA_DE_JUEGOS);
        intent.putExtra(TIPO_DE_BUSQUEDA, BUSQUEDA_POR_ORDEN);;
        intent.putExtra(MainActivity.ORDEN, orden);

        activarTransicion(intent);
    }


    private void activarTransicion(Intent intent){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }else{
            startActivity(intent);
        }
    }

    private void actualizarTab(int posicion){
        tabs.selectTab(tabs.getTabAt(posicion));
    }

    private void referenciarViews(){
        tabs = findViewById(R.id.tabLayout);
        toolbar = findViewById(R.id.include2);
    }

    private void configData(){
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                cambiarFragmentTab(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void cambiarFragmentTab(int posicion){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        switch (posicion){
            case TAB_INICIO:
                transaction.replace(R.id.contenedorFragmentTabs, Fragment_Inicio.class, null);
                break;
            case TAB_BUSCAR:
                transaction.replace(R.id.contenedorFragmentTabs, Fragment_Buscar.class, null);
                break;
            case TAB_FAVORITOS:
                transaction.replace(R.id.contenedorFragmentTabs, Fragment_Favoritos.class, null);
                break;
            default:
                break;
        }
        transaction.setReorderingAllowed(true);
        transaction.commit();
    }

    private void habilitarTransiciones(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setExitTransition(new AutoTransition());
        }
    }

    //--------------------------------------------------------------------------------------//
    private TabLayout tabs;
    private Toolbar toolbar;

    //constantes
    private final int TAB_INICIO = 0;
    private final int TAB_BUSCAR = 1;
    private final int TAB_FAVORITOS = 2;

    public final static String ABRIR_JUEGO_EN_DETALLE = "abrirjuego";
    public final static String ABRIR_LISTA_DE_JUEGOS = "abrirlistadejuegos";
    public final static String ACCION = "accion";
    public final static String TIPO_DE_BUSQUEDA = "tipo";
    public final static String BUSQUEDA_POR_GENERO = "porgenero";
    public final static String BUSQUEDA_POR_NOMBRE = "pornombre";
    public final static String GENERO = "genero";
    public final static String NOMBRE = "nombre";
};