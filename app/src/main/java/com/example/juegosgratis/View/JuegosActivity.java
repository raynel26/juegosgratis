package com.example.juegosgratis.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.view.MenuItem;
import android.view.Window;

import com.example.juegosgratis.ViewModel.JuegosViewModel;
import com.example.juegosgratis.R;

public class JuegosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //habilito transiciones para esta actividad
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new AutoTransition());
            //getWindow().setExitTransition(new Slide());
        }
        setContentView(R.layout.activity_juegos);
        viewModel = new ViewModelProvider(this).get(JuegosViewModel.class);
        inicializarViews();
        configViews();
        decidirAccion();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    private void decidirAccion(){
        Intent intentRecibido = getIntent();
        String accion = intentRecibido.getStringExtra(MainActivity.ACCION);

        switch (accion){
            case MainActivity.ABRIR_JUEGO_EN_DETALLE:
                int idJuego = intentRecibido.getIntExtra(JuegosActivity.ID_JUEGO, -1);
                abrirDetalleJuegopoId(idJuego, FAVORITOS);
                break;
            case MainActivity.ABRIR_LISTA_DE_JUEGOS:
                iniciarLista();
                break;
            default:
                break;
        }
    }

    public void abrirDetalleJuegopoId(int idJuego, String origen) {
        Bundle bundle = new Bundle();
        bundle.putInt(JuegosActivity.ID_JUEGO, idJuego);
        boolean anadirAPila;
        if(origen.equals(FAVORITOS)){
            anadirAPila = false;
        }else {
            anadirAPila = true;
        }
        iniciarFragmentJuegoEnDetalle(bundle, anadirAPila);
    }

    public void abrirDetalleJuego(Integer indiceJuego) {
        viewModel.setIndiceJuegoSeleccionadoEnListaActual(indiceJuego);
        iniciarFragmentJuegoEnDetalle(null, true);
    }

    private void iniciarLista(){
        iniciarFragmentListaDeJuegos();
    }

    private void iniciarFragmentListaDeJuegos(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.replace(R.id.contenedorPrincipal, Fragment_ListaDeJuegos.class, null);
        fragmentTransaction.commit();
    }

    private void iniciarFragmentJuegoEnDetalle(Bundle bundle, boolean agregarApila){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setReorderingAllowed(true);
        if(agregarApila){
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.contenedorPrincipal, Fragment_JuegoEnDetalle.class, bundle);
        fragmentTransaction.commit();
    }

    private void inicializarViews(){
        toolbar = findViewById(R.id.toolbarActivity);
    }

    private void configViews(){
        setSupportActionBar(toolbar);
    }

    //--------------------------------------//
    private Toolbar toolbar;
    private JuegosViewModel viewModel;

    public static String INTENT_RECIBIDO = "intentrecibido";
    public static String ID_JUEGO = "intentrecibido";
    public static String ORIGEN = "origen";
    public static String FAVORITOS = "favoritos";
    public static String LISTA = "lista";
}