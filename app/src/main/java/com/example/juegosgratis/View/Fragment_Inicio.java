package com.example.juegosgratis.View;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.AdaptadorRecyclerViewGenerico;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.Holders.BotonesViewDataHolder;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.Holders.JuegosPopularesViewDataHolder;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.OnclickItemListener;
import com.example.juegosgratis.ViewModel.MainViewModel;
import com.example.juegosgratis.View.Adapters.AdapterPager_ImagenesDeslizables;
import com.example.juegosgratis.R;
import com.example.juegosgratis.Model.ConstantesJuego;
import com.example.juegosgratis.Model.Juego.Juego;


import java.util.ArrayList;
import java.util.List;

public class Fragment_Inicio extends Fragment implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        Log.d(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        return inflater.inflate(R.layout.fragment__inicio, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        referenciarViews(view);
        configData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d(TAG, "onViewStateRestored");
        //int item = savedInstanceState.getInt("item");
        //_viewPager.setCurrentItem(item);
    }

    public void nada(){}

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        //Integer item = _viewModel.getItemJuegosAlazar();
        //if(item != null){
        //    _viewPager.setCurrentItem(item);
        //}
    }

    private void referenciarViews(View v){
        _viewModel.setContext(getContext());
        _viewPager = v.findViewById(R.id.vp_ImagenesDezlizables);
        _rvListaBotonesGeneros = v.findViewById(R.id.rv_listaBotonesGeneros);
        _rvListaJuegosPopulares = v.findViewById(R.id.rv_listaDeJuegosPopulares);
        //_rvListaCardsPorgenero = findViewById(R.id.rv_listaDeCardsPorGenero);
        tv_verMasPopulares = v.findViewById(R.id.verMasPopulares);
        punto1 = v.findViewById(R.id.punto1);
        punto2 = v.findViewById(R.id.punto2);
        punto3 = v.findViewById(R.id.punto3);
    }

    private void configData(){
        if(_viewModel.getListaDeJuegosAleatorios() == null && _viewModel.getListaJuegosMasPopulares() == null){
            _viewModel.obtenerListaDeJuegosMasPopulares();
            _viewModel.obtenerObservableListaJuegosMasPopulares().observe(getViewLifecycleOwner(), new Observer<List<Juego>>() {
                @Override
                public void onChanged(List<Juego> listaJuegosMasPopulares) {
                    ArrayList<Juego> listaDeJuegosAleatorios = (ArrayList<Juego>) _viewModel.filtrarTresJuegosAleatorios(listaJuegosMasPopulares);
                    cargarImagenesDeslizables(listaDeJuegosAleatorios);

                    ArrayList<Juego> listaDeLosDiezMasPopulares = (ArrayList<Juego>) _viewModel.filtrarPrimerosDiezJuegos(listaJuegosMasPopulares);
                    cargarListaDeJuegosPopulares(listaDeLosDiezMasPopulares);
                }
            });
        }else{
            cargarImagenesDeslizables(_viewModel.getListaDeJuegosAleatorios());
            cargarListaDeJuegosPopulares(_viewModel.getListaJuegosMasPopulares());
        }
        mostrarBotonesPorGenero();
        punto1.setOnClickListener(this);
        punto2.setOnClickListener(this);
        punto3.setOnClickListener(this);
    }

    private void cargarImagenesDeslizables(List<Juego> listaJuegos){
        AdapterPager_ImagenesDeslizables adapterPager = new AdapterPager_ImagenesDeslizables(listaJuegos, getContext());
        _viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position){
                    case IMAGEN_DEZLIZABLE_1:
                        pintarPunto1();
                        break;
                    case IMAGEN_DEZLIZABLE_2:
                        pintarPunto2();
                        break;
                    case IMAGEN_DEZLIZABLE_3:
                        pintarPunto3();
                        break;
                    default:
                        break;
                }
            }
        });
        _viewPager.setAdapter(adapterPager);
    }

    private void cargarListaDeJuegosPopulares(List<Juego> listaJuegos){
        AdaptadorRecyclerViewGenerico<Juego> adapter = new AdaptadorRecyclerViewGenerico<>(getContext(), listaJuegos, new JuegosPopularesViewDataHolder());
        adapter.setIdLayout(R.layout.presentacion_mas_popular);
        adapter.setOnClickItems(new OnclickItemListener() {
            @Override
            public void onClickItem(View v, int posicion) {
                MainActivity activity = (MainActivity) requireActivity();
                activity.abrirDetalleDeJuegoPorId(listaJuegos.get(posicion).getId());
            }
        });
        tv_verMasPopulares.setOnClickListener(this);
        _rvListaJuegosPopulares.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        _rvListaJuegosPopulares.setAdapter(adapter);
    }

    private void mostrarBotonesPorGenero() {
        _rvListaBotonesGeneros.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        List<String> generos = _viewModel.getListaDeGenerosReducida();
        AdaptadorRecyclerViewGenerico<String> adaptador = new AdaptadorRecyclerViewGenerico<>(getContext(), generos, new BotonesViewDataHolder());
        adaptador.setIdLayout(R.layout.boton_genero);
        adaptador.setOnClickItems(new OnclickItemListener() {
            @Override
            public void onClickItem(View v, int posicion) {
                MainActivity activity = (MainActivity) requireActivity();
                activity.abrirListaDeJuegosPorGenero(generos.get(posicion));
            }
        });
        _rvListaBotonesGeneros.setAdapter(adaptador);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == punto1.getId()){
            _viewPager.setCurrentItem(IMAGEN_DEZLIZABLE_1);
            pintarPunto1();
        }
        if(v.getId() == punto2.getId()){
            _viewPager.setCurrentItem(IMAGEN_DEZLIZABLE_2);
            pintarPunto2();
        }
        if(v.getId() == punto3.getId()){
            _viewPager.setCurrentItem(IMAGEN_DEZLIZABLE_3);
            pintarPunto3();
        }
        if(v.getId() == tv_verMasPopulares.getId()){
            MainActivity activity = (MainActivity) requireActivity();
            activity.abrirListaDeJuegosPorOrden(ConstantesJuego.MAS_POPULARES);
        }
    }

    private void pintarPunto1(){
        punto1.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.purple_500));
        punto2.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.gris));
        punto3.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.gris));
    }

    private void pintarPunto2(){
        punto2.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.purple_500));
        punto1.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.gris));
        punto3.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.gris));
    }

    private void pintarPunto3(){
        punto3.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.purple_500));
        punto2.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.gris));
        punto1.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.gris));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
        _viewModel.setItemJuegosAlazar(_viewPager.getCurrentItem());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    private MainViewModel _viewModel;
    private ViewPager2 _viewPager;
    private RecyclerView _rvListaBotonesGeneros;
    private RecyclerView _rvListaJuegosPopulares;
    private ImageView punto1, punto2, punto3;
    private TextView tv_verMasPopulares;
    String TAG = "FRAGMENT INICIO";

    private final int IMAGEN_DEZLIZABLE_1 = 0;
    private final int IMAGEN_DEZLIZABLE_2 = 1;
    private final int IMAGEN_DEZLIZABLE_3 = 2;
}