package com.example.juegosgratis.View.Adapters.AdaptadorGenerico.Holders;

import android.view.View;
import android.widget.TextView;

import com.example.juegosgratis.R;
import com.example.juegosgratis.Model.Juego.Juego;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.ViewData;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class JuegosPopularesViewDataHolder extends ViewData<Juego> {
    @Override
    public void referenciarViews(View itemView) {
        imageView = itemView.findViewById(R.id.imagen_JuegoPopular);
        titulo =  itemView.findViewById(R.id.titulo_juegoPopular);
        genero =  itemView.findViewById(R.id.genero_JuegoPopular);
        plataforma =  itemView.findViewById(R.id.plataforma_JuegoPopular);
    }

    @Override
    public void setData(Juego juego) {
        Picasso.with(imageView.getContext()).load(juego.getThumbnail()).into(imageView);
        titulo.setText(juego.getTitle());
        genero.setText(juego.getGenre());
        plataforma.setText(juego.getPlatform());
    }

    private CircleImageView imageView;
    private TextView titulo;
    private TextView genero;
    private TextView plataforma;
}

