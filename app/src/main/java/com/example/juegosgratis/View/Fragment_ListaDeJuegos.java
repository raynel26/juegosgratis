package com.example.juegosgratis.View;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.example.juegosgratis.View.Adapters.AdaptadorListaDeJuegos;
import com.example.juegosgratis.ViewModel.JuegosViewModel;
import com.example.juegosgratis.R;
import com.example.juegosgratis.Util.Filtros;
import com.example.juegosgratis.Model.Juego.Juego;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class  Fragment_ListaDeJuegos extends Fragment implements View.OnClickListener, SearchView.OnCloseListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(LOG_TAG, "onCreate");
        viewModel = new ViewModelProvider(requireActivity()).get(JuegosViewModel.class);
        viewModel.setContext(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreateView");
        return inflater.inflate(R.layout.fragment_lista_de_juegos, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        Log.v(LOG_TAG, "onViewCreated");
        referenciarViews(v);
        desplegarLista();
    }

    public void abrirDetalleJuego(Integer indiceJuego) {
        JuegosActivity activity = (JuegosActivity)requireActivity();
        activity.abrirDetalleJuegopoId(indiceJuego, JuegosActivity.LISTA);
    }

    public void abrirDetalleJuego(Juego juego) {
        JuegosActivity activity = (JuegosActivity)requireActivity();
        activity.abrirDetalleJuegopoId(juego.getId(), JuegosActivity.LISTA);
    }

    private void desplegarLista(){
        List<Juego> listaDeJuegosActual = viewModel.obtenerListaDeJuegosActual();
        if(listaDeJuegosActual == null){
            Intent intentRecibido = getActivity().getIntent();
            String tipoDeBusqueda = intentRecibido.getExtras().getString(MainActivity.TIPO_DE_BUSQUEDA);
            switch (tipoDeBusqueda) {
                case MainActivity.BUSQUEDA_POR_GENERO:
                    String genero = intentRecibido.getExtras().getString(MainActivity.GENERO);
                    busquedaPorGenero(genero);
                    break;
                case MainActivity.BUSQUEDA_POR_NOMBRE:
                    String nombre = intentRecibido.getExtras().getString(MainActivity.NOMBRE);
                    busquedaPorNombre(nombre);
                    break;
                case MainActivity.BUSQUEDA_POR_ORDEN:
                    String orden = intentRecibido.getExtras().getString(MainActivity.ORDEN);
                    busquedaPorOrden(orden);
                    break;
                default:
                    break;
            }
        }else{
            configViews(listaDeJuegosActual);
        }
    }

    public void busquedaPorOrden(String orden){
        viewModel.obtenerListaDeJuegosPorOrden(orden);
        viewModel.getObservableListaDeJuegos().observe(getViewLifecycleOwner(), new Observer<List<Juego>>() {
            @Override
            public void onChanged(List<Juego> juegos) {
                configViews(juegos);
            }
        });
    }

    public void busquedaPorGenero(String genero){
        if (genero.equals("All Category")) {
            viewModel.obtenerListaDeTodosLosJuegos();
        } else {
            viewModel.obtenerListaDeJuegosPorGenero(genero);
        }
        viewModel.getObservableListaDeJuegos().observe(getViewLifecycleOwner(), new Observer<List<Juego>>() {
            @Override
            public void onChanged(List<Juego> listaDeJuegos) {
                //viewModel.setListaDeJuegosActual(listaDeJuegos);
                configViews(listaDeJuegos);
            }
        });
    }

    public void busquedaPorNombre(String nombre){
        viewModel.obtenerListaDeTodosLosJuegos();
        viewModel.getObservableListaDeJuegos().observe(getViewLifecycleOwner(), new Observer<List<Juego>>() {
            @Override
            public void onChanged(List<Juego> listaDeJuegos) {
                listaDeJuegos = Filtros.filtrarListaDeJuegosPorNombre(listaDeJuegos, nombre);
                //viewModel.setListaDeJuegosActual(listaDeJuegos);
                configViews(listaDeJuegos);
            }
        });
    }

    public JuegosViewModel getViewModel() {
        return viewModel;
    }

    public void desplegarMensaje(String mensaje){
        Snackbar.make(getView(), mensaje, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    private void referenciarViews(View v){
        rvListaDeJuegos = v.findViewById(R.id.rv_listaDeJuegos);
        toolbar = v.findViewById(R.id.toolbar);
        searchView = v.findViewById(R.id.searchListaDeJuegos);
    }

    private void configViews(List<Juego> listaDeJuegos){
        rvListaDeJuegos.setLayoutManager(new LinearLayoutManager(getContext()));
        rvListaDeJuegos.setAdapter(new AdaptadorListaDeJuegos(this, listaDeJuegos, getContext()));
        searchView.setOnSearchClickListener(this);
        searchView.setOnCloseListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == searchView.getId()){
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) searchView.getLayoutParams();
            layoutParams.width = 0;
            layoutParams.horizontalBias = (float) 0.0;
            searchView.setLayoutParams(layoutParams);
        }
    }

    @Override
    public boolean onClose() {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) searchView.getLayoutParams();
        layoutParams.width = (int)getResources().getDimension(R.dimen.tamañoViewEnToolbar);
        layoutParams.horizontalBias = (float) 0.973;
        searchView.setLayoutParams(layoutParams);
        return false;
    }

    //----------------------------------------------------------------------------------------------//
    private JuegosViewModel viewModel;
    private RecyclerView rvListaDeJuegos;
    private RecyclerView rvListaDeFiltros;
    private Toolbar toolbar;
    private SearchView searchView;


    private final String LOG_TAG = "FRAGMENT_LISTA";
}