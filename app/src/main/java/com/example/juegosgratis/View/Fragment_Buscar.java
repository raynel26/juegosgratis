package com.example.juegosgratis.View;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.juegosgratis.View.JuegosActivity;
import com.example.juegosgratis.View.MainActivity;
import com.example.juegosgratis.ViewModel.MainViewModel;
import com.example.juegosgratis.R;

public class Fragment_Buscar extends Fragment implements SearchView.OnQueryTextListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        viewModel.obtenerTodosLosJuegos();
        Log.v(LOG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreateView");
        return inflater.inflate(R.layout.fragment__buscar, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        referenciarViews(view);
        configViews(view);
        Log.v(LOG_TAG, "onViewCreate");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.v(LOG_TAG, "onCreateView");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v(LOG_TAG, "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.v(LOG_TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(LOG_TAG, "onDestroy");
    }

    private void referenciarViews(View v){
        rv_vistosRecientemente = v.findViewById(R.id.rv_vistosRecientemente);
        tv_vistoRecientemente = v.findViewById(R.id.tv_vistosRecientemente);
        searchView = v.findViewById(R.id.searchView);
    }

    private void configViews(View v){
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        abrirListaDeJuegosPorNombre(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    public void abrirListaDeJuegosPorNombre(String nombre){
        Intent intent = new Intent(getContext(), JuegosActivity.class);
        intent.putExtra(MainActivity.ACCION, MainActivity.ABRIR_LISTA_DE_JUEGOS);
        intent.putExtra(MainActivity.TIPO_DE_BUSQUEDA, MainActivity.BUSQUEDA_POR_NOMBRE);
        intent.putExtra(MainActivity.NOMBRE, nombre);
        activarTransicion(intent);
    }

    public void abrirListaDeJuegosBuscados(){}

    private void activarTransicion(Intent intent){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(requireActivity()).toBundle());
        }else{
            startActivity(intent);
        }
    }

    private RecyclerView rv_vistosRecientemente;
    private TextView tv_vistoRecientemente;
    private MainViewModel viewModel;
    private SearchView searchView;
    String LOG_TAG = "FRAGMENT_BUSCAR";
}