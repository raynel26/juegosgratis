package com.example.juegosgratis.View.Adapters.AdaptadorGenerico.Holders;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juegosgratis.R;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.ViewData;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class JuegoFavoritoViewDataHolder extends ViewData<JuegoParaFavorito> {
    @Override
    public void referenciarViews(View itemView) {
        imagenJuego = itemView.findViewById(R.id.cim_imagenCircular);
        nombreJuego = itemView.findViewById(R.id.tv_nombreJuegoFavorito);
        eliminar = itemView.findViewById(R.id.ibt_eliminar);
    }

    @Override
    public void setData(JuegoParaFavorito juegoParaFavorito) {
        Picasso.with(imagenJuego.getContext()).load(juegoParaFavorito.getImagen()).into((ImageView) imagenJuego);
        nombreJuego.setText(juegoParaFavorito.getNombre());
    }
    private CircleImageView imagenJuego;
    private TextView nombreJuego;
    private ImageButton eliminar;
}
