package com.example.juegosgratis.View;

import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juegosgratis.ViewModel.JuegosViewModel;
import com.example.juegosgratis.R;
import com.example.juegosgratis.Model.Juego.JuegoConDetalles;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;

public class Fragment_JuegoEnDetalle extends Fragment implements View.OnClickListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            setEnterTransition(new Slide(Gravity.RIGHT));
        }

        Log.v(LOG_TAG, "onCreate");
        viewModel = new ViewModelProvider(requireActivity()).get(JuegosViewModel.class);
        controladorDescripcionDesplegada = false;
        controladorRequisitosDesplegada = false;
        viewModel.getObservableJuegoEnDetalle().observe(this, new Observer<JuegoConDetalles>() {
            @Override
            public void onChanged(JuegoConDetalles fullGame) {
                //viewModel.setJuegoEnDetalle(fullGame);
                configVistas(fullGame);
                //viewModel.agregarAVistosRecientemente(fullGame.getGame());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_juego_en_detalle, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        Log.v(LOG_TAG, "onViewCreated");
        referenciarVistas(v);
        Bundle bundle = getArguments();
        int idJuego = bundle.getInt(JuegosActivity.ID_JUEGO);
        viewModel.obtenerJuegoEnDetalle(idJuego);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.v(LOG_TAG, "onViewStateRestored");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.v(LOG_TAG, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(LOG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(LOG_TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v(LOG_TAG, "onStop");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        //viewModel.nullearJuegoSeleccionado();
        super.onDestroyView();
        Log.v(LOG_TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(LOG_TAG, "onDestroy");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == botonDesplegarDescripcion.getId()) {
            desplegarEsconderDescripcion();
        } else {
            if (v.getId() == botonDesplegarRequisitos.getId()) {
                desplegarEsconderRequisitos();
            }else {
                ArrayList<ImageView> arrayImagenes = new ArrayList<>();
                arrayImagenes.addAll(imagenes);
                for (int i = 0; i < arrayImagenes.size(); i++) {
                    if(v.getId() == arrayImagenes.get(i).getId()){
                        expandirImagen(arrayImagenes, i);
                    }
                }
            }
        }
    }

    private void expandirImagen(ArrayList<ImageView> arrayImagenes, int posicion){
        FragmentManager fragmentManager = getParentFragmentManager();

        Bundle datos = new Bundle();
        datos.putInt(IMAGEN_SELECCIONADA, posicion);

        fragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.contenedorPrincipal, Fragment_ImagenExpandida.class, datos)
                .addToBackStack(null)
                .commit();
    }

    private void desplegarEsconderDescripcion() {
        if (controladorDescripcionDesplegada) {
            botonDesplegarDescripcion.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_24));
            contenedorDescripcion.setVisibility(View.GONE);
            controladorDescripcionDesplegada = false;
        } else {
            botonDesplegarDescripcion.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_24));
            contenedorDescripcion.setVisibility(View.VISIBLE);
            controladorDescripcionDesplegada = true;
        }
    }

    private void desplegarEsconderRequisitos() {
        if (controladorRequisitosDesplegada) {
            botonDesplegarRequisitos.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_24));
            contenedorRequisitos.setVisibility(View.GONE);
            controladorRequisitosDesplegada = false;
        } else {
            botonDesplegarRequisitos.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_24));
            contenedorRequisitos.setVisibility(View.VISIBLE);
            controladorRequisitosDesplegada = true;
        }
    }

    private void referenciarVistas(View v) {
        botonDesplegarDescripcion = v.findViewById(R.id.desplegarDescripcion);
        botonDesplegarRequisitos = v.findViewById(R.id.desplegarRequisitos);
        botonDesplegarDescripcion.setOnClickListener(this);
        botonDesplegarRequisitos.setOnClickListener(this);

        imagenPrincipal = v.findViewById(R.id.imagenPrincipalCircular);
        titulo = v.findViewById(R.id.tituloJuegoEnDetalle);
        descripcion = v.findViewById(R.id.tv_descripconArrellenar);
        os = v.findViewById(R.id.tv_osMinimosRequisitosArellenar);
        procesador = v.findViewById(R.id.tv_processorRequisitosMinimosArellenar);
        memoria = v.findViewById(R.id.tv_memoryRequisitosMinimosArellenar);
        graficos = v.findViewById(R.id.tv_grapicsRequisitosMinimosArellenar);
        espacioRequerido = v.findViewById(R.id.tv_storageRequisitosMinimosArellenar);

        cardContenedorImagenes = v.findViewById(R.id.imagenesDetallesJuegos);
        contenedorImagenes = v.findViewById(R.id.contenedorImagenesDeJuegoEnDetalle);
        imagenes = new LinkedList<>();

        contenedorDescripcion = v.findViewById(R.id.vg_ventanaDescripcion);
        contenedorRequisitos = v.findViewById(R.id.vg_ventanaMinimosRequisitos);
    }

    private void configVistas(JuegoConDetalles fullGame) {
        configVistasDetallesGenerales(fullGame);
        configVistasDescripcion(fullGame);
        configVistasRequisitos(fullGame);
        configVistasImagenes(fullGame);
    }

    private void configVistasDetallesGenerales(JuegoConDetalles fullGame) {
        Picasso.with(getActivity()).load(fullGame.getGame().getThumbnail()).into(imagenPrincipal);
        titulo.setText(fullGame.getGame().getTitle());
    }

    private void configVistasDescripcion(JuegoConDetalles fullGame) {
        descripcion.setText(fullGame.getDescripcion());
    }

    private void configVistasRequisitos(JuegoConDetalles fullGame) {
        os.setText(fullGame.getRequisitosMinimos().getOs());
        procesador.setText(fullGame.getRequisitosMinimos().getProcessor());
        memoria.setText(fullGame.getRequisitosMinimos().getMemory());
        graficos.setText(fullGame.getRequisitosMinimos().getGraphics());
        espacioRequerido.setText(fullGame.getRequisitosMinimos().getStorage());
    }

    private void configVistasImagenes(JuegoConDetalles fullGame) {
        if (fullGame.getImagenes().size() > 0) {
            int cantidadImagenes = fullGame.getImagenes().size();

            for (int i = 0; i < cantidadImagenes; i++) {
                ImageView imageViewNueva = new ImageView(getContext());
                imageViewNueva.setId(ViewCompat.generateViewId());
                ConstraintLayout.LayoutParams parametros = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.alturaImagen));
                if (i == 0) {
                    parametros.startToStart = ConstraintLayout.LayoutParams.PARENT_ID;
                    parametros.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;
                    parametros.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
                } else {
                    parametros.startToStart = ConstraintLayout.LayoutParams.PARENT_ID;
                    parametros.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;
                    parametros.topToBottom = imagenes.get(i - 1).getId();
                }
                imageViewNueva.setLayoutParams(parametros);
                Picasso.with(getActivity()).load(fullGame.getImagenes().get(i).getImage()).into(imageViewNueva);
                contenedorImagenes.addView(imageViewNueva);
                imageViewNueva.setOnClickListener(this);
                imagenes.addLast(imageViewNueva);
            }
            cardContenedorImagenes.setVisibility(View.VISIBLE);
        }
    }

    private JuegosViewModel viewModel;

    private Button botonDesplegarDescripcion;
    private Button botonDesplegarRequisitos;

    private ImageView imagenPrincipal;
    private TextView titulo;
    private TextView descripcion;
    private TextView os;
    private TextView procesador;
    private TextView memoria;
    private TextView graficos;
    private TextView espacioRequerido;

    private CardView cardContenedorImagenes;
    private ConstraintLayout contenedorImagenes;
    private LinkedList<ImageView> imagenes;

    private ConstraintLayout contenedorDescripcion;
    private ConstraintLayout contenedorRequisitos;
    private ConstraintLayout contenedorImagen;
    private Boolean controladorDescripcionDesplegada;
    private Boolean controladorRequisitosDesplegada;

    public String LOG_TAG = "testJuegoEnDetalle";
    public static String IMAGEN_SELECCIONADA = "posicion";

}