package com.example.juegosgratis.View.Adapters.AdaptadorGenerico;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que implementa un Adapter de RecyclerView "generico" que ahorra la implementacion repetitiva de muchos Adapters
 *, implementa escuchadores a cada view que recicla el RecyclerView y metodos tipicos.
 *
 * @param <Objeto> tipo de datos que contendra la lista
 */
public class AdaptadorRecyclerViewGenerico<Objeto> extends RecyclerView.Adapter<AdaptadorRecyclerViewGenerico.Holder> {

    public AdaptadorRecyclerViewGenerico(Activity activity, List<Objeto> lista) {
        this.activity = activity;
        this.lista = lista;
        vistasClickeables = new ArrayList<>();
    }

    public AdaptadorRecyclerViewGenerico(Context context, List<Objeto> lista, ViewData viewData) {
        this.context = context;
        this.lista = lista;
        this.viewData = viewData;
        vistasClickeables = new ArrayList<>();
    }

    public void setIdLayout(int idLayout){
        this.idLayout = idLayout;
    }

    public void setElementoClickeable(int idView){

        vistasClickeables.add(idView);
    }

    public void setOnClickItems(OnclickItemListener onClickItems){
        itemsClickeables = true;
        this.onClickListener = onClickItems;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(idLayout, parent, false);
        Holder holder = new Holder(view, viewData);
        if(itemsClickeables){
            view.setOnClickListener(holder);
        }
        if(!vistasClickeables.isEmpty()){
            for (int i = 0; i < vistasClickeables.size(); i++) {
                View v = view.findViewById(vistasClickeables.get(i));
                v.setOnClickListener(holder);
            }
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(AdaptadorRecyclerViewGenerico.Holder holder, int position) {
        holder.referenciarViews();
        holder.setData(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void eliminarItem(int posicion){
        lista.remove(posicion);
        notifyItemRemoved(posicion);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Holder(View view, ViewData data) {
            super(view);
            this.view = view;
            data.referenciarViews(view);
        }

        @Override
        public void onClick(View v) {
            onClickListener.onClickItem(v, getAdapterPosition());
        }

        public void referenciarViews(){
            viewData.referenciarViews(view);
        }

        public void setData(Objeto objeto){
            viewData.setData(objeto);
        }

        private View view;
    }

    private ArrayList<Integer> vistasClickeables;
    private boolean itemsClickeables = false;
    private int idLayout;
    private Context context;
    private Activity activity;
    private List<Objeto> lista;
    private OnclickItemListener onClickListener;
    private ViewData viewData;
}
