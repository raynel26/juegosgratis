package com.example.juegosgratis.View;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.AdaptadorRecyclerViewGenerico;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.Holders.JuegoFavoritoViewDataHolder;
import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.OnclickItemListener;
import com.example.juegosgratis.ViewModel.MainViewModel;
import com.example.juegosgratis.R;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.ArrayList;

public class Fragment_Favoritos extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment__favoritos, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        referenciarViews(view);
        configViews(view);
    }

    public void abrirDetalleDeJuegoPorId(int idJuego){
        MainActivity activityHost = (MainActivity) requireActivity();
        activityHost.abrirDetalleDeJuegoPorId(idJuego);
    }

    public void eliminarJuegoDeFavoritos(int idJuego){
        viewModel.eliminarJuegoDeFavotitos(idJuego);
    }

    private void referenciarViews(View view){
        rv_listaDeJuegosFavoritos = view.findViewById(R.id.rv_listaJuegosFavoritos);
    }

    private void configViews(View view){
        ArrayList<JuegoParaFavorito> listaJuegosFavoritos = viewModel.obtenerFavoritos();
        AdaptadorRecyclerViewGenerico<JuegoParaFavorito> adapter = new AdaptadorRecyclerViewGenerico<>(getContext(), listaJuegosFavoritos, new JuegoFavoritoViewDataHolder());
        adapter.setIdLayout(R.layout.card_favorito);
        adapter.setElementoClickeable(R.id.ibt_eliminar);
        adapter.setOnClickItems(new OnclickItemListener() {
            @Override
            public void onClickItem(View v, int posicion) {
                if(v.getId() == R.id.ibt_eliminar){
                    JuegoParaFavorito juegoParaFavorito = listaJuegosFavoritos.get(posicion);
                    eliminarJuegoDeFavoritos(juegoParaFavorito.getId());
                    adapter.eliminarItem(posicion);
                }else{
                    abrirDetalleDeJuegoPorId(listaJuegosFavoritos.get(posicion).getId());
                }
            }
        });
        rv_listaDeJuegosFavoritos.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_listaDeJuegosFavoritos.setAdapter(adapter);
        //rv_listaDeJuegosFavoritos.setLayoutManager(new LinearLayoutManager(getContext()));
        //rv_listaDeJuegosFavoritos.setAdapter(new AdaptadorJuegoFavorito(this, listaJuegosFavoritos));
    }

    private MainViewModel viewModel;
    private RecyclerView rv_listaDeJuegosFavoritos;
}