package com.example.juegosgratis.View.Adapters.AdaptadorGenerico;

import android.view.View;

public interface OnclickItemListener {
    public void onClickItem(View v, int posicion);
}
