package com.example.juegosgratis.View.Adapters.AdaptadorGenerico;

import android.view.View;

/**
 * Clase que utiliza la clase "AdaptadorRecyclerViewGenerico" para referenciar los views de cada item y poder acceder a ellos
 * para modificar conforme el recyclerView lo requiera
 * @param <Objeto> Tipo de datos que contiene el modelo de datos que presentara cada item.
 */
public abstract class ViewData<Objeto> {

    public ViewData() {

    }

    public abstract void referenciarViews(View itemView);

    public abstract void setData(Objeto objeto);
}
