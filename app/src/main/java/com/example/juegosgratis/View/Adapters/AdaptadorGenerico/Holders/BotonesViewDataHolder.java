package com.example.juegosgratis.View.Adapters.AdaptadorGenerico.Holders;

import android.view.View;
import android.widget.Button;

import com.example.juegosgratis.View.Adapters.AdaptadorGenerico.ViewData;
import com.example.juegosgratis.R;

public class BotonesViewDataHolder extends ViewData<String> {
    public BotonesViewDataHolder() {
    }

    @Override
    public void referenciarViews(View itemView) {
        botonGenero = itemView.findViewById(R.id.bt_genero);
    }

    @Override
    public void setData(String categoria) {
        botonGenero.setText(categoria);
    }

    private Button botonGenero;

}
