package com.example.juegosgratis.View.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.juegosgratis.View.Fragment_ListaDeJuegos;
import com.example.juegosgratis.R;
import com.example.juegosgratis.Util.Busqueda;
import com.example.juegosgratis.Model.Juego.Juego;
import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdaptadorListaDeJuegos extends RecyclerView.Adapter<AdaptadorListaDeJuegos.JuegoHolder> {


    public AdaptadorListaDeJuegos(Fragment_ListaDeJuegos fragment, List<Juego> listaJuegos, Context context) {
        this.fragment = fragment;
        this.listaJuegos = listaJuegos;
        listaDeFavoritos = fragment.getViewModel().getFavoritos();
        this.context = context;
    }

    @Override
    public JuegoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if(viewType == VIEWTITULO){
            v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.textview, parent, false);
            vistaCantidadDeElementos = v;
        }else{
            v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.card_presentaciondejuego, parent, false);
        }
        return new JuegoHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorListaDeJuegos.JuegoHolder holder, int position) {
        int viewType = getItemViewType(position);
        if(viewType == VIEWTITULO){
            holder.setCantidad();
        }else{
            Juego juego = listaJuegos.get(position - 1);
            holder.setDatosDeJuego(juego);
            holder.getView().setOnClickListener(holder);
            holder.get_botonFavorito().setOnClickListener(holder);
        }

    }

    @Override
    public int getItemCount() {
        return listaJuegos.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return VIEWTITULO;
        }else{
            return VIEWELEMENTOS;
        }
    }

    public void abrirDetalleJuego(int posicion){
        fragment.abrirDetalleJuego(listaJuegos.get(posicion));
    }

    protected void agregarAFavoritos(Juego juego){
        listaDeFavoritos.add(new JuegoParaFavorito(juego.getThumbnail(), juego.getTitle(), juego.getId()));
        fragment.getViewModel().agregarAFavoritos(juego);
        char comillas = (char)34;
        fragment.desplegarMensaje(comillas + juego.getTitle() + comillas + " agregado a favoritos");
    }

    protected void eliminarDeFavoritos(Juego juego, int indiceEnListaDeFavoritos){
        listaDeFavoritos.remove(indiceEnListaDeFavoritos);
        fragment.getViewModel().eliminarJuegoDeFavoritos(juego.getId());
        char comillas = (char)34;
        fragment.desplegarMensaje(comillas + juego.getTitle() + comillas + " eliminado de favoritos");
    }

    private boolean esFavorito(Juego juego){
        int indice = Busqueda.busquedaLineal(listaDeFavoritos, juego.getId());
        return indice != -1;
    }


    private void pintarCorazon(AppCompatImageButton botonFavorito){
        Resources resources = fragment.getResources();
        botonFavorito.setBackground(ContextCompat.getDrawable(context, R.drawable.favorito_on));
        botonFavorito.setSupportBackgroundTintList(ContextCompat.getColorStateList(context, R.color.rojo));
    }

    private void despintarCorazon(AppCompatImageButton botonFavorito){
        botonFavorito.setBackground(ContextCompat.getDrawable(context, R.drawable.favorito_off));
        botonFavorito.setSupportBackgroundTintList(ContextCompat.getColorStateList(context, R.color.black));
    }


    class JuegoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public JuegoHolder(View itemView) {
            super(itemView);
            v = itemView;
            if(itemView == vistaCantidadDeElementos){
                _tvCantDeElementosEncontrados = itemView.findViewById(R.id.tv);
            }else{
                _imagenJuego = itemView.findViewById(R.id.cardImagenJuego);
                _tituloJuego = itemView.findViewById(R.id.cardtv_titulo);
                _generoJuego = itemView.findViewById(R.id.cardtv_genero);
                _plataformaJuego = itemView.findViewById(R.id.cardtv_plataforma);
                _descripcionCortaJuego = itemView.findViewById(R.id.cardtv_descripcionCorta);
                _botonFavorito = itemView.findViewById(R.id.bt_favorito);
            }
        }

        public void setDatosDeJuego(Juego juego) {
            setImagenJuego(juego.getThumbnail());
            setTituloJuego(juego.getTitle());
            setGeneroJuego(juego.getGenre());
            setPlataformaJuego(juego.getPlatform());
            setDescripcionCortaJuego(juego.getShort_description());
            if(esFavorito(juego)){
                pintarCorazon(_botonFavorito);
            }else{
                despintarCorazon(_botonFavorito);
            }
        }

        private void setImagenJuego(String linkImagen) {
            Picasso.with(fragment.getActivity()).load(linkImagen).into(_imagenJuego);
        }

        private void setTituloJuego(String tituloJuego) {
            _tituloJuego.setText(tituloJuego);
        }

        private void setGeneroJuego(String generoJuego) {
            _generoJuego.setText(fragment.getResources().getString(R.string.cardGenero) + " " + generoJuego);
        }

        private void setPlataformaJuego(String plataformaJuego) {
            _plataformaJuego.setText(fragment.getResources().getString(R.string.cardPlataforma) + " " + plataformaJuego);
        }

        private void setDescripcionCortaJuego(String descripcionCortaJuego) {
            _descripcionCortaJuego.setText(fragment.getResources().getString(R.string.cardDescripcion) + " " + descripcionCortaJuego);
        }

        public void setCantidad() {
            _tvCantDeElementosEncontrados.setText(listaJuegos.size() + " elementos encontrados");
        }

        public View getView() {
            return v;
        }

        @Override
        public void onClick(View v) {
            int posicion = getAdapterPosition() - 1;
            if(v.getId() == _botonFavorito.getId()){
                Juego juego = listaJuegos.get(posicion);
                //si el juego no esta en la lista de favoritos entonces el resultado es -1, si no, el result es el indice
                int indice = Busqueda.busquedaLineal(listaDeFavoritos, juego.getId());
                if(indice != -1 ){
                    eliminarDeFavoritos(juego, indice);
                    _botonFavorito.setBackground(fragment.getResources().getDrawable(R.drawable.favorito_off));
                    despintarCorazon(_botonFavorito);
                }else{
                    agregarAFavoritos(juego);
                    pintarCorazon(_botonFavorito);
                }
            }else{
                abrirDetalleJuego(posicion);
            }
        }

        public ImageButton get_botonFavorito() {
            return _botonFavorito;
        }

        private ImageView _imagenJuego;
        private AppCompatImageButton _botonFavorito;
        private TextView _tituloJuego;
        private TextView _generoJuego;
        private TextView _plataformaJuego;
        private TextView _descripcionCortaJuego;
        private TextView _tvCantDeElementosEncontrados;
        private View v;
    }

    private List<JuegoParaFavorito> listaDeFavoritos;
    private Fragment_ListaDeJuegos fragment;
    private List<Juego> listaJuegos;
    private View vistaCantidadDeElementos;
    private Context context;
    private int VIEWTITULO = 0;
    private int VIEWELEMENTOS = 1;
}
