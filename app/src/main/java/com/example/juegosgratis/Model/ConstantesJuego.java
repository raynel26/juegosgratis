package com.example.juegosgratis.Model;

public class ConstantesJuego {
    public static final String MAS_POPULARES = "popularity";
    public static final String MAS_RELEVANTES = "relevance";
    public static final String FECHA = "fecha";
    public static final String ALFABETICO = "alphabetical";
}
