package com.example.juegosgratis.Model.Juego;

import java.util.List;

public class JuegoConDetalles {

    public JuegoConDetalles(){}

    public JuegoConDetalles(Juego game, RequisitosMinimos requisitosMinimos, List<ScreenShot> imagenes, String descripcion){
        this.game = game;
        this.requisitosMinimos = requisitosMinimos;
        this.imagenes = imagenes;
        this.descripcion = descripcion;
    }

    public RequisitosMinimos getRequisitosMinimos() {
        return requisitosMinimos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public List<ScreenShot> getImagenes() {
        return imagenes;
    }

    public Juego getGame() {
        return game;
    }

    public void setGame(Juego game) {
        this.game = game;
    }

    public void setRequisitosMinimos(RequisitosMinimos requisitosMinimos) {
        this.requisitosMinimos = requisitosMinimos;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setImagenes(List<ScreenShot> imagenes) {
        this.imagenes = imagenes;
    }

    private Juego game;
    private RequisitosMinimos requisitosMinimos;
    private String descripcion;
    private List<ScreenShot> imagenes;
}
