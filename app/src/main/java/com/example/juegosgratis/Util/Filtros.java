package com.example.juegosgratis.Util;

import com.example.juegosgratis.ViewModel.MainViewModel;
import com.example.juegosgratis.Model.Juego.Juego;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Filtros {

    public static List<Juego> filtrasTresJuegosAleatorios(List<Juego> listaDeJuegos){
        //busco tres juegos de manera aleatoria entre todos los de la lista de juegos
        LinkedList<Integer> indicesAleatorios = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            indicesAleatorios.add(random.nextInt(listaDeJuegos.size()));
        }
        ArrayList<Juego> res = new ArrayList<>();
        //agrego los tres juegos elegidos al azar a la lista de juegos a mostrar
        for (int i: indicesAleatorios) {
            res.add(listaDeJuegos.get(i));
        }
        return res;
    }

    public static List<Juego> filtrarPrimerosDiezJuegos(List<Juego> listaDeJuegosPopulares){
        ArrayList<Juego> res = new ArrayList<>(MainViewModel.CANTIDAD_JUEGOS_POPULARES);
        int longitud = listaDeJuegosPopulares.size();
        for (int i = 0; i < MainViewModel.CANTIDAD_JUEGOS_POPULARES && i < longitud; i++) {
            res.add(i, listaDeJuegosPopulares.get(i));
        }
        return res;
    }

    public static List<Juego> filtrarListaDeJuegosPorNombre(List<Juego> juegos, String nombre){
        LinkedList<Juego> listaDeJuegosFiltrados = new LinkedList<>();
        for (Juego j: juegos) {
            if(j.getTitle().toLowerCase().contains(nombre.toLowerCase())){
                listaDeJuegosFiltrados.add(j);
            }
        }
        return new ArrayList<>(listaDeJuegosFiltrados);
    }

}
