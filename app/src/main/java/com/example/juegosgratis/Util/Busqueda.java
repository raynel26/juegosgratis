package com.example.juegosgratis.Util;

import com.example.juegosgratis.Model.Juego.JuegoParaFavorito;

import java.util.List;

public class Busqueda {

    public static int busquedaLineal(List<JuegoParaFavorito> lista, int id){
        int i = 0;
        int longitud = lista.size();
        while (i < longitud && (lista.get(i).getId() != id)) {
            i++;
        }
        if(i < lista.size()){
            return i;
        }else{
            return -1;
        }
    }
}
